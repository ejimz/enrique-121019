# base image
FROM node:12.11.1-alpine

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app


# set working directory
WORKDIR /home/node/app

# Default root user could implies security issues, we'll use node user
USER node

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY --chown=node:node ./entrypoint.sh /home/node/

# install and cache app dependencies
COPY --chown=node:node ./package.json /home/node/app/package.json

COPY --chown=node:node ./ /home/node/app

EXPOSE 3000

RUN npm install

# start app
ENTRYPOINT ["/home/node/entrypoint.sh"]

CMD ["web"]
