# Enrique - 121019

## Installation


#### Get repository and move into it
```
git clone https://gitlab.com/ejimz/enrique-121019.git
cd enrique-121019
```

#### Build the container
```
docker-compose build
```

#### Run container
```
docker-compose up -d
```

#### Check container status
```
docker-compose ps
```

#### Visit the next websites

React demo: http://reactdemo.localhost
LOGS: http://logs.reactdemo.localhost 

Proxy dashboard: http://localhost:8080


## Checklist
- [X] Can run app within Docker without installing Node.js on host
- [X] Rebuilding the Docker image will install any changes to Node modules
- [X] Changing a file will automatically rebuild app within Docker
   - [X] Not including changes to Node modules (i.e. package.json & package-lock.json)
   - [X] For example: examples/todomvc/src/components/Header.js 
         Change “todos” to “Kraken Todos” then refresh web browser to review.
- [X] Continuous integration
  - [X] Use Docker images for runners.
  - [X] Build the app.
  - [X] Run the tests and generate code coverage repo.
- [X] Review App for app
  - [X] Review Apps for both:
    - [X] Demo React App
    - [X] Code Coverage Report
  - [X] Review Apps are separated per branch
    - [X] You should be able to simultaneously view Review Apps for the `master` branch
          and a Merge Request with a different branch name
  - [X] Use AWS S3 (free tier) for hosting the static files
  - [X] Demonstrate it works by creating a small Merge Requests with a change

## Demo

Credentials
```
User: demo
Password: react

```

**Merge Request:**  https://gitlab.com/ejimz/enrique-121019/merge_requests/8

**App Review main page:** https://dajbwtyxylr27.cloudfront.net/feature-headermod/index.html

**App Review App:** https://dajbwtyxylr27.cloudfront.net/feature-headermod/build/index.html

**Coverage Review App:** https://dajbwtyxylr27.cloudfront.net/feature-headermod/coverage/lcov-report/index.html

## Features

#### Enabled debug port

Enabled 9229 port in local dev environment for nodejs debugging. it improves the development experience with code editors.

#### Using traefik
Using traefik as proxy http implies connect the website via dns instead of port which improve the development experience.

#### Using Logio
I have added a logio container which collect the logs of the containers and show it in a website, it improves the development experience.

#### Gitignore
Using gitignore to not commit unnecessary data to the repo.

#### Dockerignore
Using dockerignore to not include unnecessary files to containers.

#### Nodemon 
Using nodemon as dev dependency in order to reload the app in case a file is modified. node_modules dir, package and package-json files are excluded in nodemon.json.

#### Using AWS S3 + Cloudfront
I use a private bucket in AWS S3 to host the site artifacts. The bucket is allowed to receive request only from our cloudfront distribution.

#### Basic AUTH and SSL
Using AWS Cloudfront and AWS Lambda to protect the sites by Basic Auth and using SSL

#### Compare feature branch with current master site

When the master branch receive a commmit it build and upload the site to S3, however this environment is not going to be deleted, 
we use it to compare with new feature branches.
When a feature branch create a merge request, the artifacts are uploaded to S3 but also a basic html file is uploaded in order
to point out the current master site and the new feature site, so the reviewer can see both sites and coverage simultaneously.

## Security
List security concerns:
- that have been addressed
  *  Node image use by default root user, I have changed it to "node" user.
  *  Using a separated docker network. 
  *  Signing commits with my personal gpg key.
  *  Storing AWS keys in gitlab variables and masking it in order to not showing the keys in logs (also the keys have only permissions to put and get files in a specific bucket).
  *  Using https for "review apps" (using cloudfront instead directly s3).
  *  Using lambda function between cloudfront and S3 to have basic http auth in order to avoid exposing the webapps to the world.

