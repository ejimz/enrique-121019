#!/bin/sh
#
set -e

if [ "$1" = 'dev' ]; then
        npm install;
        npm run dev;
elif [ "$1" = 'test' ]; then
	npm run build;
	npm run test;
elif [ "$1" = 'web' ]; then
        npm start;
else
        exec "$@"
fi

